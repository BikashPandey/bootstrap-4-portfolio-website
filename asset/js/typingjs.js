var app = document.getElementById('banner-role');

var typewriter = new Typewriter(app, {
    loop: true
});

typewriter.pauseFor(1500)
    .typeString('I Am Bikash Pandey')
    .pauseFor(2500)
    .deleteChars(13)
    .typeString('a Student')
    .pauseFor(2500)
    .deleteChars(7)
    .typeString('Web Designer')
    .pauseFor(2500)
    .deleteChars(8)
    .typeString('Developer')
    .pauseFor(2500)
    .start();
